
(function () {
    'use strict';

    // Translate Provider
    function configDynamicLocale (tmhDynamicLocaleProvider, PATH_CONFIG) {

        tmhDynamicLocaleProvider.localeLocationPattern(PATH_CONFIG.EXTEND_BOWER + 'angular-i18n/angular-locale_{{locale}}.js');

    }

    configDynamicLocale.$inject =['tmhDynamicLocaleProvider', 'PATH_CONFIG'];

    angular
        .module('bravoureAngularApp')
        .config(configDynamicLocale);

})();
