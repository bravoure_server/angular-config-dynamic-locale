# Bravoure - Config Dynamic Locale

## Use

It is used to set the base to set up for Dynamic Locale (i18n)


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-config-dynamic-locale": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    
or in the root of the project execute:
    
    ./update
